package com.immobel.invoiceservice.infrastructure.repository;

import com.immobel.invoiceservice.domain.model.Invoice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface InvoiceRepo extends CrudRepository<Invoice, String>
{
    //TODO return list for the possibility of finding more than one
    public Optional<List<Invoice>> findByInvoiceStatus(String invoiceStatus);

    public Optional<Invoice> findByInvoiceNumber(String number);

}
