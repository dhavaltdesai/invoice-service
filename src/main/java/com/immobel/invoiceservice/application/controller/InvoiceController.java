package com.immobel.invoiceservice.application.controller;


import com.immobel.invoiceservice.application.InvoiceAppService;
import com.immobel.invoiceservice.application.InvoiceAppService;
import com.immobel.invoiceservice.domain.model.Invoice;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;
import java.util.Optional;

@RestController
public class InvoiceController {

    @Autowired
    private InvoiceAppService invoiceAppService;

    @PostMapping("/api/invoice")
    public ResponseEntity<Invoice> createInvoice(@RequestBody Invoice invoice)
    {
        Invoice createdInvoice = invoiceAppService.createNewInvoice(invoice);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .contentType(MediaType.APPLICATION_JSON)
                .body(createdInvoice);



    }

    @GetMapping("/api/invoice/{invoicenumber}")
    public ResponseEntity<Invoice> getInvoiceById(@PathVariable("invoicenumber") String invoiceNumber)
    {
        Optional<Invoice> fromDB = invoiceAppService.findById(invoiceNumber);
        if(fromDB.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromDB.get());
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();

        }



    }

    @PutMapping("/api/invoice")
    public ResponseEntity<Invoice> updateInvoice(@RequestBody Invoice invoice)
    {

        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(invoiceAppService.update(invoice));
    }

    @DeleteMapping("/api/invoice/{number}")
    public ResponseEntity deleteInvoiceByCode(@PathVariable("number") String invoiceNumber)
    {
        invoiceAppService.deleteInvoice(invoiceNumber);
        return ResponseEntity
                .status(HttpStatus.OK)
                .build();
    }

    @GetMapping
    public HttpStatus readynessCheck()
    {
        return HttpStatus.OK;
    }

    @GetMapping("/api/invoicenumber")
    public ResponseEntity<Invoice> getInvoiceByCode(@RequestParam("invoicenumber") String invoiceNumber )
    {
        Optional<Invoice> fromDB = invoiceAppService.findByInvoiceCode(invoiceNumber);

        if(fromDB.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromDB.get());
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();

        }

    }

    @GetMapping("/api/invoicequery")
    public ResponseEntity<List<Invoice>> getAllInvoiceByStatus(@RequestParam("status") String status)
    {
        Optional<List<Invoice>> fromDB = invoiceAppService.getAllInvoiceByStatus(status);

        if(fromDB.isPresent())
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(fromDB.get());
        }
        else
        {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .build();

        }
    }
}