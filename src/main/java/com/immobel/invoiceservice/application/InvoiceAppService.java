package com.immobel.invoiceservice.application;

import com.immobel.invoiceservice.domain.model.Invoice;
import com.immobel.invoiceservice.domain.model.Invoice;
import com.immobel.invoiceservice.infrastructure.repository.InvoiceRepo;
import com.immobel.invoiceservice.infrastructure.repository.InvoiceRepo;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class InvoiceAppService {

    @Autowired
    private InvoiceRepo invoiceRepo;

    public Invoice createNewInvoice(Invoice invoice)
    {
        return invoiceRepo.save(invoice);
    }

    public Optional<Invoice> findById(String invoiceId)
    {
        Optional<Invoice> invoiceFound = invoiceRepo.findById(invoiceId);
        return invoiceFound.isPresent()?invoiceFound:Optional.empty();

    }

    public Optional<Invoice> findByInvoiceCode(String invoiceCode)
    {
        Optional<Invoice> invoiceFound = invoiceRepo.findByInvoiceNumber(invoiceCode);
        return invoiceFound.isPresent()?invoiceFound:Optional.empty();

    }

    public Invoice update(Invoice invoice)
    {
        return invoiceRepo.save(invoice);
    }

    //TODO : replace void by some kind of confirmation back to user
    public void deleteInvoice(String invoiceId)
    {
        invoiceRepo.deleteById(invoiceId);
    }

    public Optional<List<Invoice>> getAllInvoiceByStatus(String status)
    {
        return invoiceRepo.findByInvoiceStatus(status);
    }


}
